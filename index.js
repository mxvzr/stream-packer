var Transform = require('stream').Transform,
	util = require('util');

function ObjectPacker(opts) {
	if (!(this instanceof ObjectPacker))
		return new ObjectPacker(opts);

	opts.objectMode = true;
	this.maxSize = opts.maxSize || 100;

	Transform.call(this, opts);
	this.buffer = [];
}

util.inherits(ObjectPacker, Transform);

ObjectPacker.prototype._transform = function(chunk, encoding, fn) {
	this.buffer.push(chunk);
	if (this.buffer.length >= this.maxSize)
		this.flush();
	fn();
};

ObjectPacker.prototype.flush = function() {
	if (!this.buffer.length) return;
	this.push(this.buffer);
	this.buffer = [];
};

ObjectPacker.prototype.end = function() {
	this.flush();
	Transform.prototype.end.apply(this, arguments);
};


module.exports = ObjectPacker;
